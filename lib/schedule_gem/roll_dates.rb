class RollDates

  def initialize(task, schedule)
    if schedule.class == Schedule
      @task = task
      @schedule = schedule
      @frequency = schedule.frequency
      @interval = schedule.interval
      @rolling_start = @schedule.rolling_start_date
      @rolling_end = @schedule.rolling_end_date
    else
      raise ArgumentError
    end
  end

  #
  # returns potentially updated version of the inputted task
  #
  def self.roll_dates!(task, schedule)
    if rd = self.new(task, schedule)
      rd.select_frequency
    end
  end

  def select_frequency
    case @frequency
    when "Daily" then roll_daily
    when "Weekly" then roll_weekly
    when "Monthly" then roll_monthly
    else
      raise RollDatesError
    end
  end

  def roll_daily
    interval = @interval.to_i
    @task.start_date = interval.days.from_now(@task.start_date) if @rolling_start
    @task.end_date = interval.days.from_now(@task.end_date) if @rolling_end
    return_task_if_saved
  end

  def roll_weekly
    @task.start_date = roll_one_week(@task.start_date) if @rolling_start
    @task.end_date = roll_one_week(@task.end_date) if @rolling_end
    return_task_if_saved
  end

  def roll_monthly
    @task.start_date = roll_one_month(@task.start_date) if @rolling_start
    @task.end_date = roll_one_month(@task.end_date) if @rolling_end
    return_task_if_saved
  end

  def roll_one_week(date)
    1.weeks.from_now(date)
  end

  def roll_one_month(date)
    1.month.from_now(date)
  end

  def return_task_if_saved
    @task.save! if @task.changed?
    @task
  end

end

class RollDatesError < RuntimeError
end