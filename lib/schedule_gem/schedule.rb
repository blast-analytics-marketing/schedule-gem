class Schedule < ActiveRecord::Base

  belongs_to :scheduleable, polymorphic: true

  before_validation :format_hour

  before_validation do |schedule|
    schedule.interval = schedule.interval.gsub(" ", "") if schedule.interval
  end
  
  validates :frequency, :presence => true
  validates :interval, :presence => true
  validate :frequency_valid?
  validate :schedule_valid?

  FREQUENCY_OPTIONS = ["Daily", "Weekly", "Monthly"]
  DAY_OPTIONS = (1..100).to_a.map{|n| "#{n}"}
  WEEK_OPTIONS = Date::DAYNAMES
  MONTH_OPTIONS = (1..31).to_a.map{|n| "#{n}"}

  scope :upcoming_weekly, -> (day) { where("frequency = ? AND interval = ?", "Weekly", day) }
  scope :end_of_month, -> (day) { where("frequency = ? AND interval >= ?", "Monthly", day) }
  scope :upcoming_monthly, -> (day) { where("frequency = ? AND interval = ?", "Monthly", day) }
  scope :only_active, -> { where("active = ?", true) }

  def to_s
    if (self.scheduleable && self.scheduleable.name)
      self.scheduleable.name
    else
      "#{created_at.strftime('%D')} #{frequency} #{interval}"
    end
  end

  def format_hour
    if self.hour
      fhour = self.hour[0..1]
      fhour = fhour.gsub(/\D/, '')
      fhour = fhour.gsub('0', '') if (fhour[0] == "0")
      self.hour = fhour
    end
  end

  def format_interval
    self.interval = self.interval.gsub(" ","")
  end

  private

    def frequency_valid?
      unless FREQUENCY_OPTIONS.include?(frequency)
        errors.add :base, "Please select a valid query schedule frequency."
      end
    end

    def schedule_valid?
      case frequency
      when "Daily"
        unless DAY_OPTIONS.include?(interval)
          errors.add :base, "When selecting a report to run on a day basis, 
          please choose an interval between 1 and 100 days."
        end
      when "Weekly"
        unless WEEK_OPTIONS.include?(interval)
          errors.add :base, "Please select a day of the week when scheduling 
          queries to run weekly."
        end
      when "Monthly"
        unless MONTH_OPTIONS.include?(interval)
          errors.add :base, "Please select which day of the month you would 
          like your queries to run when scheduling by month."
        end
      else
        true
      end
    end

end