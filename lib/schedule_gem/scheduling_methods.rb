module ScheduleGem

	module ScheduleGemMethods
	  extend ActiveSupport::Concern
	 
	  module ClassMethods

	  	def set_time(time)
	  		time = Time.now unless time
	  		@current_time = time
	  		@month = @current_time.strftime('%B')
	  		@day_of_week = @current_time.strftime('%A')
	  		@day = @current_time.strftime('%e').gsub(' ', '')
	  		@last_day_of_month = @current_time.end_of_month.strftime('%e') == @day
	  	  @hour = @current_time.strftime('%k').gsub(' ', '')
	  	end

	  	def raise_incompatibility_error
	  		raise "It seems like you're trying to call methods related to scheduling without declaring the relationship first. Try including 'has_one :schedule' in your model."
	  	end

	  	#
	  	# the following methods query all schedules to run on the @current_day and 
	  	# adds their corresponding AnalyticQuery records to an array. 
	  	#
	  	# @return [Active Record Relation] of records that have schedules as a relation
	  	#
	  	def daily_tasks(&block)
	  		check_scheduling_compatability
	  		tasks = includes(:schedule).where(schedules:{frequency: "Daily", active: true})
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def daily_tasks_with_hours(&block)
	  	  tasks = daily_tasks.records_with_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def daily_tasks_without_hours(&block)
	  	  tasks = daily_tasks.records_without_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def daily_tasks_by_hour(&block)
	  	  set_time(nil)
	  	  tasks = daily_tasks_with_hours.select(
	  	      &filter_current_hour_in_timezone
	  	    )
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	#
	  	# weekly schedules
	  	#

	  	def weekly_tasks(time=nil, &block)
	  		check_scheduling_compatability
	  		set_time(time)
	  		tasks = includes(:schedule).where(schedules:{frequency: "Weekly", interval: @day_of_week, active: true})
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def weekly_tasks_with_hours(time=nil, &block)
	  	  tasks = weekly_tasks(time).records_with_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def weekly_tasks_without_hours(time=nil, &block)
	  	  tasks = weekly_tasks(time).records_without_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def weekly_tasks_by_hour(time=nil, &block)
	  	  tasks = weekly_tasks_with_hours(time).select(
	  	      &filter_current_hour_in_timezone
	  	    )
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	#
	  	# monthly schedules
	  	#

	  	def monthly_tasks(time=nil, &block)
	  		check_scheduling_compatability
	  		set_time(time)
	  		if @last_day_of_month
	  			tasks = joins(:schedule).where("schedules.frequency = ? AND schedules.interval >= ? AND schedules.active = ?", "Monthly", @day, true)
	  		else
	  			tasks = joins(:schedule).where("schedules.frequency = ? AND schedules.interval = ? AND schedules.active = ?", "Monthly", @day, true)
	  		end
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def monthly_tasks_with_hours(time=nil, &block)
	  	  tasks = monthly_tasks(time).records_with_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def monthly_tasks_without_hours(time=nil, &block)
	  	  tasks = monthly_tasks(time).records_without_specified_hours
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	def monthly_tasks_by_hour(time=nil, &block)
	  	  tasks = monthly_tasks_with_hours(time).select(
	  	      &filter_current_hour_in_timezone
	  	    )
	  	  yield_tasks_to_block(tasks, &block)
	  	end

	  	#
	  	# helper methdos
	  	#

	  	def check_scheduling_compatability
	  	  raise_incompatibility_error unless reflect_on_association(:schedule)
	  	end

	  	def records_with_specified_hours
	  	  where.not(schedules: {hour: nil})
	  	end

	  	def records_without_specified_hours
	  	  where(schedules: {hour: nil})
	  	end

	  	#
	  	# yeilds both the task and the schedule record.
	  	#

	  	def yield_tasks_to_block(tasks, &block)
	  	  if block_given? && tasks.respond_to?(:each)
	  	    tasks.each {|task| yield(task, task.schedule)}
	  	  else
	  	    tasks
	  	  end
	  	end

	  	def current_hour_in_timezone(time_zone)
	  	  if time_zone
	  	    @current_time.in_time_zone(time_zone).hour.to_s
	  	  else
	  	    @hour
	  	  end
	  	  rescue ArgumentError
	  	    @hour
	  	end

	  	def filter_current_hour_in_timezone
	  	  lambda{ |task|
	  	    current_hour_in_timezone(task.schedule.try(:time_zone)) == task.schedule.hour
	  	  }
	  	end

	  end
	  
	end
	ActiveRecord::Base.send(:include, ScheduleGemMethods)
end