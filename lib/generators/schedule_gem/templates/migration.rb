class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
    	t.integer :scheduleable_id
    	t.string :scheduleable_type
    	t.string :frequency
    	t.string :interval
    	t.boolean :active, :default => true, :null => false
      t.boolean :rolling_start_date, :default => false, :null => false
      t.boolean :rolling_end_date, :default => false, :null => false
      t.string :hour, :default => nil
      t.string :time_zone, :default => nil
      t.timestamps
    end
  end
end
