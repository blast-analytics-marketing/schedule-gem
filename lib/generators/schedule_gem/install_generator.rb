require 'rails/generators'

module ScheduleGem
	module Generators
		class InstallGenerator < Rails::Generators::Base
		  source_root File.expand_path('../templates', __FILE__)

		  def generate_schedule_table_migration
		  	prefix = Time.now.utc.strftime("%Y%m%d%H%M%S")
		  	template "migration.rb", "db/migrate/#{prefix}_create_schedules.rb"
		  end
		end
	end
end
