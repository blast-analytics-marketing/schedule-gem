# ScheduleGem

This gem contains a migration and corresponding model for a scheduling table / class. It also contains an extention for active record that allows the easy quering of records that need to preform scheduled tasks. It is testing in the context of the Unsampler application. 

## Installation

Add this line to your application's Gemfile:
    
	1) Install the Gem!	
    gem 'schedule_gem', git: "https://naysayer@bitbucket.org/blast-analytics-marketing/schedule-gem.git"

	2) And then run bundle to install:
    $ bundle

	3) Run the generator 
    $ rails generate schedule_gem:install

	4) Migrate!
    $ rake db:migrate
    
    5) Define relationships
    Within the rails models you want to have scheduling methods add:
    has_one :schedule, as: :scheduleable

## Usage

####Daily scheduled methods

    ModelName.daily_tasks(&block)
    ModelName.daily_tasks_with_hours(&block)
    ModelName.daily_tasks_without_hours(&block)
    ModelName.daily_tasks_by_hour(&block)

####Weekly scheduled methods
-Weekly methods can take a time stamp as an optional argument, doing so will return records within the scope of that date. For example if the current day was a Thursday, and you provided a timestamp with a week day of Monday, then it would return records that would be performed on a Monday, not Thrusday.

-You can pass these methods a block, which yeilds two variables, the task itself as well as the schedule record. 

    ModelName.weekly_tasks(time=nil, &block)
    ModelName.weekly_tasks_with_hours(time=nil, &block)
    ModelName.weekly_tasks_without_hours(time=nil, &block)
    ModelName.weekly_tasks_by_hour(time=nil, &block)

####Monthly scheduled methods
-Monthly methods can take a time stamp as an optional argument, doing so will return records within the scope of that date. For example if the current date was the 6th day of the month, and you provided a timestamp with for the first day of the month, then it would return records that would be performed on the 1st day of the month, not the 6th


    ModelName.monthly_tasks(time=nil, &block)
    ModelName.monthly_tasks_with_hours(time=nil, &block)
    ModelName.monthly_tasks_without_hours(time=nil, &block)
    ModelName.monthly_tasks_by_hour(time=nil, &block)
    
####Basic usage
-All of the methods stated above will accept a block. The block will yeild two arguments, the "task" and the schedule record. The task being the record that the schedule is associated to. 

    Task.monthly_tasks do |task_record, schedule_record|
        if task_record.bad?
            schedule_record.active = false
        else
            task_record.do_something!
        end
    end
    
####FYI
- Monthly tasks: Overhanging tasks will be run on the last day of the month. For example Febuary 28th will return / run all month records that have a date of 28th - 31st

## Dependencies / included gems 

- Rails

## Contributing

1. Fork it ( https://github.com/[my-github-username]/schedule_gem/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
